import config
import json
import platform
import tools
#
from flask import Flask, send_from_directory
from relays import RelayControl, RelayOperating
from relayer import get_config
#
from pprint import PrettyPrinter
pp = PrettyPrinter()

app = Flask(__name__, static_folder='static', static_url_path='')

relay_control = RelayControl(
    bank_qty=config.RELAY_BANKS, bank_pop=config.RELAY_BANK_SIZE, test_count=0)

relay_config = get_config(platform.uname().node, relay_control.get_state())
group_state = {group: False for group in relay_config['groups']}


def get_group_info():
    return group_state


def get_device_info():
    current_state = relay_control.get_state()
    bank_state = dict()
    for bank, bank_relays in enumerate(current_state):
        this_bank = dict()
        this_bank['id'] = bank
        this_bank['name'] = relay_config['banks'][str(bank)]['name']
        these_relays = relay_config['banks'][str(bank)]['relays']
        relays = dict()
        for relay, relay_state in enumerate(bank_relays):
            this_relay = dict()
            this_relay['id'] = relay
            this_relay['name'] = (these_relays[str(relay)]['name'])
            this_relay['state'] = (relay_state == RelayOperating)
            this_relay['groups'] = [
                group for group in these_relays[str(relay)]['groups']]
            relays[relay] = this_relay
        this_bank['relays'] = relays
        bank_state[bank] = this_bank
    return bank_state


@app.route('/', methods=['GET'])
def serve_landing_page():
    return send_from_directory('static', 'index.html')


@app.route('/api/device/state', methods=['GET'])
def get_state():
    state = {
        'name': relay_config['name'],
        'groups': get_group_info(),
        'relayBanks': get_device_info(),
    }
    return state


@app.route('/api/device/set/<int:bank>/<int:relay>/<state>', methods=['GET'])
def set_relay_state(bank, relay, state):
    relay_control.set_relay_tx(bank, relay, state.lower())
    return get_device_info()


@app.route('/api/device/byGroup/<group>/<state>', methods=['GET'])
def set_relay_state_by_group(group, state):
    entries = {u: v for u, v in {bank['id']: {relay['id']: relay['groups']
                                              for relay in bank['relays'].values()
                                              if group in relay['groups']}
               for bank in relay_config['banks'].values()}.items()
               if v}
    for bank, relays in entries.items():
        for relay in relays.keys():
            group_state[group] = relay_control.set_relay_tx(
                bank, relay, state.lower())
    return get_device_info()


# @app.route('/api/device/byName/<bank>/<relay>/<state>', methods=['GET'])
# def set_relay_state_by_name(bank, relay, state):
#     relay_control.set_relay_tx(int(bank[-1]), int(relay[-1]), state.lower())
#     return get_device_info()


@app.route('/<path:path>', methods=['GET'])
def serve_static_content(path):
    return send_from_directory('static', path)


if __name__ == '__main__':
    app.run()
