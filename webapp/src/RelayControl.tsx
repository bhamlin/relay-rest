import { useCallback, useEffect, useState } from "react";
import { web_service_base } from "./config";
import { Button, Col, Container, Row, Stack } from 'react-bootstrap';

type StateSetCallback<T> = React.Dispatch<React.SetStateAction<T>>;

type Relay = { id: number, name: string, state: boolean, groups: Array<string> };
type RelayStates = { [key: string]: Relay };
type RelayBank = { id: number, name: string, relays: RelayStates }
type RelayBankStates = { [key: string]: RelayBank };
type GroupStates = { [key: string]: boolean };
type OverallState = { ready: boolean, name: string, groups: GroupStates, relayBanks: RelayBankStates };

const API_STATE = web_service_base + '/state'

const getRelayState = () => (
    fetch(API_STATE).then(resp => resp.json())
);

const buttonIntermediateState = (entity: HTMLButtonElement) => {
    const cl = entity.classList;
    cl.remove("btn-outline-success");
    cl.remove("btn-danger")
    cl.add("btn-info");
}

const postGroupStateChange = (entity: HTMLButtonElement, group: string, state: boolean) => {
    buttonIntermediateState(entity);

    fetch(`${web_service_base}/byGroup/${group}/${'' + state}`);
};

const postRelayStateChange = (entity: HTMLButtonElement, bank: number,
    relay: number, state: boolean) => {
    buttonIntermediateState(entity);

    fetch(`${web_service_base}/set/${bank}/${relay}/${'' + state}`);
};

const updateRelayState = (update: StateSetCallback<OverallState>) => (
    getRelayState().then(data => {
        update({ ready: true, ...data });
    })
);

const RelayView = ({ bank_id, relay }: { bank_id: number, relay: Relay }) => (
    <Button variant={relay.state ? 'danger' : 'outline-success'} size="lg"
        onClick={({ target }) => postRelayStateChange(
            target as HTMLButtonElement, bank_id, relay.id, !relay.state)}>
        {relay.state ? 'Operating' : 'Normal'}
    </Button>
);

const RelayBankView = ({ bank_id, relay }: { bank_id: number, relay: Relay }) => (
    <Container fluid className="relay-row">
        <Row className="align-items-center">
            <Col xs={3}>
                Relay: {relay.name}
            </Col>
            <Col xs={3}>
                <RelayView key={relay.id} bank_id={bank_id} relay={relay} />
            </Col>
            <Col>
                <Stack direction="horizontal" gap={3}>
                    <div className='ms-auto'></div>
                    {relay.groups.map((e) => (
                        <div key={`${bank_id}-${relay.id}-${e}`}>{e}</div>
                    ))}
                </Stack>
            </Col>
        </Row>
    </Container >
);

const RelayBankListView = ({ bank }: { bank: RelayBank }) => (
    <Container fluid className="bank-row">
        <Row className="align-items-center">
            <Col xs={2}>
                Bank: {bank.name}
            </Col>
            <Col>
                {Object.entries(bank.relays).map(([relay_id, relay_state]) => (
                    <RelayBankView key={relay_id} bank_id={bank.id} relay={relay_state} />
                ))}
            </Col>
        </Row>
    </Container >
);

const GroupControls = ({ groups }: { groups: GroupStates }) => (
    <Container fluid className="bank-row">
        <Row className="align-items-center">
            {Object.entries(groups).map(([k, v]) => (
                <Col key={k + '-col'}>
                    <Button key={k + '-button'} variant={v ? 'danger' : 'outline-success'}
                        onClick={({ target }) => postGroupStateChange(
                            target as HTMLButtonElement, k, !v)}>
                        {k}
                    </Button>
                </Col>
            ))}
        </Row>
    </Container>
);

export const RelayControl = () => {
    const [state, setState] = useState({ ready: false } as OverallState);

    const update = useCallback(() => updateRelayState(setState), []);

    useEffect(() => {
        update();
        const handle = setInterval(() => update(), 1000);
        return () => clearInterval(handle);
    }, [update]);

    if (state.ready) {
        return (
            <Stack gap={3}>
                <div>
                    <span>Relay controller for </span>
                    <code>{state.name}</code>
                </div>
                <GroupControls groups={state.groups} />
                {Object.entries(state.relayBanks).map(([bank_id, bank_state]: [string, RelayBank]) => (
                    <RelayBankListView key={bank_id} bank={bank_state} />
                ))}
            </Stack>
        );
    } else {
        return (
            <Stack gap={3}>
                <div className="bank-row">Loading...</div>
            </Stack>
        )
    }
};
