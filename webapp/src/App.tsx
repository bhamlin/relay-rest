import { Col, Container, Row } from 'react-bootstrap';
import { RelayControl } from './RelayControl';
// import React from 'react';
// import logo from './logo.svg';
import './App.css';

// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => (
  <Container fluid className="App">
    <Row><Col>&nbsp;</Col></Row>
    <Row>
      <Col>
        <h1>Pi Relay controller</h1>
        {/* <p className="lead">Relay controller</p> */}
      </Col>
    </Row>
    <Row><Col><hr /></Col></Row>
    <Row><Col><RelayControl /></Col></Row>
  </Container>
);

export default App;
