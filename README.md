# Relay REST Interface

Python and React server and webapp for controlling relays via i²c on a Raspberry Pi.

## Getting started

You must create a `config.py` from `config.py.template` (or by hand). Relevant values are:
* `RELAY_BANKS`: Number of banks of relays. Currently expects numbering to start at `0x10` and to increment by `0x01` per bank.
* `RELAY_BANK_SIZE`: Quantity of relays per bank. Default is `4`.
