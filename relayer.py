import tools

CONFIG_PATH = 'relay-config.json'


class Relay:
    def __init__(self, bank: int, id: int, state: bool = False, name: str = None, groups: "list[str]" = None) -> None:
        self.bank = bank
        self.id = id
        self.state = state
        if name:
            self.name = name
        else:
            self.name = f'relay_{id}'
        if groups:
            self.groups = [group for group in groups]
        else:
            self.groups = list()


class RelayBank:
    def __init__(self, id: int, name: str = None, groups: "list[str]" = None) -> None:
        self.relays = list()
        self.id = id
        if name:
            self.name = name
        else:
            self.name = f'relay_{id}'
        if groups:
            self.groups = [group for group in groups]
        else:
            self.groups = list()

    def add_relay(self, state: bool = False, name: str = None, groups: "list[str]" = None):
        self.relays.append(
            Relay(self.id, len(self.relays), state, name, groups))


class RelayDevice:
    def __init__(self, name: str = None, groups: "list[str]" = None) -> None:
        self.banks = list()
        self.id = id
        if name:
            self.name = name
        else:
            self.name = f'relay_{id}'
        if groups:
            self.groups = [group for group in groups]
        else:
            self.groups = list()

    def add_bank(self, state: bool = False, name: str = None, groups: "list[str]" = None):
        self.relays.append(
            Relay(self.id, len(self.relays), state, name, groups))


def get_config(name: str, state: list) -> dict:
    if not tools.file_exists(CONFIG_PATH):
        relay_config = {
            'name': name,
            'groups': {
                'link-check': False,
                'generation': False, 'interruptables': False,
                'load-reduction': False, 'substation': False,
                'water-heaters-large': False, 'water-heaters-small': False,
            },
        }
        for bank, bank_state in enumerate(state):
            relay_config[bank] = {
                'id': bank,
                'name': f'bank_{bank}',
                'groups': list(),
                'relays': dict(),
            }
            for relay in range(len(bank_state)):
                relay_config[bank]['relays'][relay] = {
                    'id': relay,
                    'name': f'relay_{relay}',
                    'groups': list(),
                }
        tools.save_json(CONFIG_PATH, relay_config, indent=2)
    return tools.load_json(CONFIG_PATH)
