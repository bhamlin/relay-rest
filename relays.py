import smbus
import time

DEFAULT_BUS = 1

RelayNormal = 0x00
RelayOperating = 0xFF

RelayBank1 = 0x10
RelayBank2 = 0x11
RelayBank3 = 0x12
RelayBank4 = 0x13

RelayBankX = [RelayBank1, RelayBank2, RelayBank3, RelayBank4]

Relay1 = 0x01
Relay2 = 0x02
Relay3 = 0x03
Relay4 = 0x04

RelayX = [Relay1, Relay2, Relay3, Relay4]


class RelayControl:
    def __init__(self, bus=DEFAULT_BUS, bank_qty=4, bank_pop=4, test_count=1) -> None:
        self.bus_id = bus
        self.bus = smbus.SMBus(self.bus_id)
        self.bank_quantity = bank_qty
        self.bank_population = bank_pop
        self.default_state = [[RelayNormal] * bank_pop] * bank_qty
        self.set_state(self.default_state)
        self.test_limit = test_count
        self.test_times = 0

        self.self_test()

    def self_test(self):
        if self.test_times >= self.test_limit:
            return

        # Test each relay
        print('Testing relay operation for all banks.')
        print('NOTICE:')
        print('  This test only verifies that the i2c bus is reporting expected values.')
        print('  This test cannot verify that the relays are in fact operating correctly.')
        for bank in range(self.bank_quantity):
            for relay in range(self.bank_population):
                print(
                    f'- Operating relay in bank {RelayBankX[bank]} / relay {RelayX[relay]}')
                u, v = RelayBankX[bank], RelayX[relay]
                assert(self.get_relay(u, v) == RelayNormal)
                print(f'  - {self.get_state()}')
                self.set_relay(u, v, RelayOperating)
                print(f'  - {self.get_state()}')
                time.sleep(1)
                assert(self.get_relay(u, v) == RelayOperating)
                self.set_relay(u, v, RelayNormal)
                print(f'  - {self.get_state()}')
        print('Completed relay operation test')

    def set_relay_tx(self, b_id, r_id, v_id):
        value = RelayNormal
        if v_id == '1':
            value = RelayOperating
        elif v_id == 'on':
            value = RelayOperating
        elif v_id == 'true':
            value = RelayOperating
        elif v_id == 'yes':
            value = RelayOperating
        self.bus.write_byte_data(RelayBankX[b_id], RelayX[r_id], value)
        return value == RelayOperating

    def set_relay(self, relay_bank, relay_id, value):
        self.bus.write_byte_data(relay_bank, relay_id, value)

    def get_relay(self, relay_bank, relay_id):
        value = self.bus.read_byte_data(relay_bank, relay_id)
        if value == 0:
            return RelayNormal
        else:
            return RelayOperating

    def get_state(self):
        state = list()
        for bank in range(self.bank_quantity):
            state.append(list())
            for relay in range(self.bank_population):
                state[bank].append(self.get_relay(
                    RelayBankX[bank], RelayX[relay]))
        return state

    def set_state(self, state):
        for bank, bank_relays in enumerate(state):
            for relay, relay_state in enumerate(bank_relays):
                self.set_relay(RelayBankX[bank], RelayX[relay], relay_state)
