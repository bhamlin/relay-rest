import json
import os
from typing import Any


def file_exists(path: str) -> bool:
    return os.path.exists(path) and not os.path.isdir(path)


def load_json(path: str, **kwargs) -> Any:
    with open(path) as FH:
        return json.load(FH, **kwargs)


def save_json(path: str, content: Any, **kwargs) -> None:
    with open(path, 'w') as FH:
        json.dump(content, FH, **kwargs)
